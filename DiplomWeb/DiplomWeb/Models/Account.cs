﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class Account
	{
		[Key]
		public int Account_ID { get; set; }
		public string FirtsName { get; set; }
		public string LastName { get; set; }
		public string Patronymic { get; set; }
		public DateTime DateOfReceipt { get; set; }
		public DateTime DateOfDismissal { get; set; }
		public int Position_ID { get; set; }
		public int Department_ID { get; set; }
		public int Role_ID { get; set; }


		public Role Role { get; set; }
		public Position Position { get; set; }
		public Department Department { get; set; }
	}
}
