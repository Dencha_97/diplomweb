﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class Test
	{
		[Key]
		public int Test_ID { get; set; }
		public string Name { get; set; } 
		public int Course_ID { get; set; }

		public Course Course { get; set; }
	}
}
