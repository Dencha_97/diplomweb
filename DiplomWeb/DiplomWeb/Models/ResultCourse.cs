﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class ResultCourse
	{
		[Key]
		public int ResultCourse_ID { get; set; }
		public int Course_ID { get; set; }
		public int Account_ID { get; set; }
		public DateTime DateStart { get; set; }
		public DateTime DateEnd { get; set; }

		public Course Course { get; set; }
		public Account Account { get; set; }
	}
}
