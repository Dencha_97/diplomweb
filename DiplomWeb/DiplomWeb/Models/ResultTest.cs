﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class ResultTest
	{
		[Key]
		public int ResultTest_ID { get; set; }
		public int Test_ID { get; set; }
		public int Account_ID { get; set; }
		public DateTime DateStart { get; set; }
		public DateTime DateEnd { get; set; }

		public Test Test { get; set; }
		public Account Account { get; set; }
	}
}
