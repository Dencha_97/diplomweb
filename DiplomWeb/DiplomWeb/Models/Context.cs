﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class Context : DbContext
	{
		public Context(DbContextOptions<Context> dbContext) : base(dbContext)
		{
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=learntesting;Username=postgres;Password=123");

            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<GenreCourse> GenreCourses { get; set; }
        public DbSet<Literature> Literatures { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<ResultCourse> ResultCourses { get; set; }
        public DbSet<ResultTest> ResultTests { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Statistics> Statistics { get; set; }
        public DbSet<Test> Tests { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.HasDefaultSchema("public");
        }

        //public static ApplicationContext Create() => new ApplicationContext();
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();

            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=learntesting;Username=postgres;Password=123", builder => builder.EnableRetryOnFailure());

            return new Context(optionsBuilder.Options);
        }
    }
}
