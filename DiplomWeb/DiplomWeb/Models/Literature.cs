﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class Literature
	{
		[Key]
		public int Literature_ID { get; set; }
		public string Name { get; set; }
		public string PathName { get; set; }
		public int GenreLiterature_ID { get; set; }
		public int Account_ID { get; set; }

		public GenreLiterature GenreLiterature { get; set; }
		public Account Account { get; set; }
	}
}
