﻿using System.ComponentModel.DataAnnotations;

namespace DiplomWeb.Models
{
	public class Role
	{
		[Key]
		public int Role_ID { get; set; }
		public string Name { get; set; }
	}
}
