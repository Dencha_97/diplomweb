﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class Statistics
	{
		[Key]
		public int Statistics_ID { get; set; }
		public int Course_ID { get; set; }
		public int Test_ID { get; set; }

		public ResultCourse Course { get; set; }
		public ResultTest Test { get; set; }
	}
}
