﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DiplomWeb.Models
{
	public class GenreLiterature
	{
		[Key]
		public int GenreLiterature_ID { get; set; }
		public string Name { get; set; }
	}
}
